// SCRIPTS FOR HEADER

"use strict";
function burgerOpen() {
  const burger = document.querySelector(".header-topmenu__burger");
  let topMenu = document.querySelector(".header-topmenu__list");
  let bl = document.querySelector(".header-topmenu__burger__line");

  burger.addEventListener("click", () => {
    bl.classList.toggle("header-topmenu__burger__line-hide");
    burger.classList.toggle("header-topmenu__burger-active");
    topMenu.classList.toggle("header-topmenu__list-active");
    document.body.classList.toggle("lock");
  });

  document.addEventListener("click", outsideEvtListener);

  function outsideEvtListener(e) {
    if (
      e.target === topMenu ||
      topMenu.contains(e.target) ||
      e.target === burger ||
      e.target === bl
    ) {
      return;
    }
    bl.classList.remove("header-topmenu__burger__line-hide");
    burger.classList.remove("header-topmenu__burger-active");
    topMenu.classList.remove("header-topmenu__list-active");
    document.body.classList.remove("lock");
  }
}
burgerOpen();
